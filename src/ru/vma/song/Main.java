package ru.vma.song;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Song song1 = new Song("Song1", "Autor1", 126); //short 120
        Song song2 = new Song("Song2", "Autor2", 500); //long 500
        Song song3 = new Song("Song3", "Autor3", 121); //medium 160
        Song song4 = new Song("Song4", "Autor4", 12); //short 12
        Song song5 = new Song("Song5", "Autor5", 500); //long 500

        Song[] songs = {song1, song2, song3, song4, song5};

        System.out.println("Вторая и пятая песни равны? - " + song2.isSameCategory(song5));

        int lenghtSec = inputSec();
        secSong(lenghtSec, songs);
        System.out.println("Short: ");
        outPutShort(songs);
    }

    /**
     * ввод продолжительности
     */
    private static int inputSec() {
        System.out.print("Продолжительность песни:");
        return scanner.nextInt();
    }

    /**
     * проверяет наличие совпадений с
     * запрашиваемой продолжительностью
     *
     * @param lenghtSec продолжительность в секундах
     * @param songs     массив песен
     */
    private static void secSong(int lenghtSec, Song[] songs) {
        boolean isHaventSong = true;
        for (Song song : songs) {
            if (lenghtSec == song.getTimeSec()) {
                System.out.println(song + " ");
                isHaventSong = false;
            }
        }
        if (isHaventSong) {
            System.out.println("такой нет");
        }
    }

    /**
     * выводит объекты с
     * продолжительностью "short"
     *
     * @param songs массив песен
     */
    private static void outPutShort(Song[] songs) {
        for (Song song : songs) {
            if (song.category().equals("short")) {
                System.out.println(song + " ");
            }
        }
    }
}