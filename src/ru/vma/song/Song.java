package ru.vma.song;

public class Song {
    private String name;
    private String autor;
    private int timeSec;

    Song(String name, String autor, int timeSec) {
        this.name = name;
        this.autor = autor;
        this.timeSec = timeSec;
    }

    /**
     * возвращает категорию песен по продолжительности
     *
     * @return одну из категорий
     */
    String category() {
        String categoryLong = "error";
        if (timeSec <= 120) {
            categoryLong = "short";
        }
        if (timeSec > 120 && timeSec <= 240) {
            categoryLong = "medium";
        }
        if (timeSec > 240) {
            categoryLong = "long";
        }
        return categoryLong;
    }

    /**
     * сравнивает текущую категорию с поступающей
     *
     * @param song однин объект из массива
     * @return одинаковые ли категории у песен
     */
    boolean isSameCategory(Song song) {
        boolean isSameCategory = false;
        if (song.category().equals(this.category())) {
            isSameCategory = true;
        }
        return isSameCategory;
    }

    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", autor='" + autor + '\'' +
                ", timeSec=" + timeSec +
                '}';
    }

    int getTimeSec() {
        return timeSec;
    }
}