package ru.vma.summa;

public class Summa {
    public static void main(String[] args) {
        int[] arr =  {28, 34, 33, 67};
        for (int value : arr) {
            System.out.println(value + " ");
        }

        int oddSum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                oddSum += arr[i];
            }
        }

        System.out.print("Сумма элементов с нечетными номерами: " + oddSum);
    }


}

